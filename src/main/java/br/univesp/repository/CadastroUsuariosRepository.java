package br.univesp.repository;

import br.univesp.domain.CadastroUsuarios;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CadastroUsuarios entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CadastroUsuariosRepository extends JpaRepository<CadastroUsuarios, Long> {
    String FIND_BY_CODIGO_RFID = "SELECT * FROM cadastro_usuarios WHERE codigo_rfid = ?1 LIMIT 1;";

    @Query(value = FIND_BY_CODIGO_RFID, nativeQuery = true)
    Optional<CadastroUsuarios> findByCodigoRfid(String codigoRfid);
}
