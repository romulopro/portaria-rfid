package br.univesp.repository;

import br.univesp.domain.UsuariosRegistrados;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UsuariosRegistrados entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsuariosRegistradosRepository extends JpaRepository<UsuariosRegistrados, Long> {
    String FIND_BY_CODIGO_RFID = "SELECT * FROM usuarios_registrados WHERE codigo_rfid = ?1 LIMIT 1;";

    @Query(value = FIND_BY_CODIGO_RFID, nativeQuery = true)
    Optional<UsuariosRegistrados> findByCodigoRfid(String rfid);
}
