import dayjs from 'dayjs/esm';

export interface ILogAcesso {
  id: number;
  email?: string | null;
  dataAcesso?: dayjs.Dayjs | null;
}

export type NewLogAcesso = Omit<ILogAcesso, 'id'> & { id: null };
