export interface IUsuariosRegistrados {
  id: number;
  email?: string | null;
  codigoRfid?: string | null;
}

export type NewUsuariosRegistrados = Omit<IUsuariosRegistrados, 'id'> & { id: null };
