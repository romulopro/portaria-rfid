import { IUsuariosRegistrados, NewUsuariosRegistrados } from './usuarios-registrados.model';

export const sampleWithRequiredData: IUsuariosRegistrados = {
  id: 18612,
  email: 'depressive ew parallel',
  codigoRfid: 'dual astonishing cemetery',
};

export const sampleWithPartialData: IUsuariosRegistrados = {
  id: 26443,
  email: 'briskly display conk',
  codigoRfid: 'what extroverted chunder',
};

export const sampleWithFullData: IUsuariosRegistrados = {
  id: 16846,
  email: 'under wherever',
  codigoRfid: 'thin silently',
};

export const sampleWithNewData: NewUsuariosRegistrados = {
  email: 'round above pivot',
  codigoRfid: 'terribly inasmuch refract',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
