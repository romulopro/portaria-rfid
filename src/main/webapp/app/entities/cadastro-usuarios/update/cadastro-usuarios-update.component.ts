import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ICadastroUsuarios } from '../cadastro-usuarios.model';
import { CadastroUsuariosService } from '../service/cadastro-usuarios.service';
import { CadastroUsuariosFormService } from './cadastro-usuarios-form.service';
import {
  UsuariosRegistradosFormGroup,
  UsuariosRegistradosFormService,
} from 'app/entities/usuarios-registrados/update/usuarios-registrados-form.service';
import { UsuariosRegistradosService } from 'app/entities/usuarios-registrados/service/usuarios-registrados.service';
import { IUsuariosRegistrados, NewUsuariosRegistrados } from 'app/entities/usuarios-registrados/usuarios-registrados.model';

@Component({
  standalone: true,
  selector: 'jhi-cadastro-usuarios-update',
  templateUrl: './cadastro-usuarios-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class CadastroUsuariosUpdateComponent implements OnInit {
  isSaving = false;
  cadastroUsuarios: ICadastroUsuarios | null = null;

  editForm: UsuariosRegistradosFormGroup = this.usuariosRegistradosFormService.createUsuariosRegistradosFormGroup();

  constructor(
    protected cadastroUsuariosService: CadastroUsuariosService,
    protected usuariosRegistradosFormService: UsuariosRegistradosFormService,
    protected usuariosRegistradosService: UsuariosRegistradosService,
    protected cadastroUsuariosFormService: CadastroUsuariosFormService,
    protected activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cadastroUsuarios }) => {
      this.cadastroUsuarios = cadastroUsuarios;
      if (cadastroUsuarios) {
        this.updateForm(cadastroUsuarios);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cadastroUsuarios = this.usuariosRegistradosFormService.getUsuariosRegistrados(this.editForm);
    const newUsuariosRegistrados: NewUsuariosRegistrados = {
      ...cadastroUsuarios,
      id: null,
      email: cadastroUsuarios.email,
    };
    this.subscribeToSaveResponse(this.usuariosRegistradosService.create(newUsuariosRegistrados));
    this.cadastroUsuariosService.delete(this.cadastroUsuarios!.id).subscribe(() => this.onSaveFinalize());
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUsuariosRegistrados>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cadastroUsuarios: ICadastroUsuarios): void {
    this.cadastroUsuarios = cadastroUsuarios;
    this.usuariosRegistradosFormService.resetForm(this.editForm, cadastroUsuarios);
  }
}
