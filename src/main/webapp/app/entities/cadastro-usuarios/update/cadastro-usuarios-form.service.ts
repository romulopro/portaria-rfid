import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ICadastroUsuarios, NewCadastroUsuarios } from '../cadastro-usuarios.model';
import { IUsuariosRegistrados, NewUsuariosRegistrados } from 'app/entities/usuarios-registrados/usuarios-registrados.model';
import { UsuariosRegistradosFormGroup } from 'app/entities/usuarios-registrados/update/usuarios-registrados-form.service';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ICadastroUsuarios for edit and NewCadastroUsuariosFormGroupInput for create.
 */
type CadastroUsuariosFormGroupInput = ICadastroUsuarios | PartialWithRequiredKeyOf<NewCadastroUsuarios>;

type CadastroUsuariosFormDefaults = Pick<NewCadastroUsuarios, 'id'>;

type CadastroUsuariosFormGroupContent = {
  id: FormControl<ICadastroUsuarios['id'] | NewCadastroUsuarios['id']>;
  codigoRfid: FormControl<ICadastroUsuarios['codigoRfid']>;
  dataInclusao: FormControl<ICadastroUsuarios['dataInclusao']>;
};

type UsuariosRegistradosFormGroupContent = {
  id: FormControl<IUsuariosRegistrados['id'] | NewUsuariosRegistrados['id']>;
  email: FormControl<IUsuariosRegistrados['email']>;
  codigoRfid: FormControl<IUsuariosRegistrados['codigoRfid']>;
};

export type CadastroUsuariosFormGroup = FormGroup<CadastroUsuariosFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class CadastroUsuariosFormService {
  createCadastroUsuariosFormGroup(cadastroUsuarios: CadastroUsuariosFormGroupInput = { id: null }): UsuariosRegistradosFormGroup {
    const cadastroUsuariosRawValue = {
      ...this.getFormDefaults(),
      codigoRfid: cadastroUsuarios.codigoRfid,
      email: '',
    };
    return new FormGroup<UsuariosRegistradosFormGroupContent>({
      id: new FormControl(
        { value: cadastroUsuariosRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      codigoRfid: new FormControl(
        { value: cadastroUsuariosRawValue.codigoRfid, disabled: true },
        {
          validators: [Validators.required],
        },
      ),
      email: new FormControl(cadastroUsuariosRawValue.email, {
        validators: [Validators.required],
      }),
    });
  }

  getCadastroUsuarios(form: CadastroUsuariosFormGroup): ICadastroUsuarios | NewCadastroUsuarios {
    return form.getRawValue() as ICadastroUsuarios | NewCadastroUsuarios;
  }

  resetForm(form: CadastroUsuariosFormGroup, cadastroUsuarios: CadastroUsuariosFormGroupInput): void {
    const cadastroUsuariosRawValue = { ...this.getFormDefaults(), ...cadastroUsuarios };
    form.reset(
      {
        ...cadastroUsuariosRawValue,
        id: { value: cadastroUsuariosRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): CadastroUsuariosFormDefaults {
    return {
      id: null,
    };
  }
}
